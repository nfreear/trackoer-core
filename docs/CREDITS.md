# trackoer-core
## Credits and licenses


### trackoer-core: Copyright (c) 2012 The Open University. All rights reserved.
* License: free/ open-source -- license to be confirmed.
* Author: Nick Freear <n.d.freear+@+open.ac.uk> / Institute of Educational Technology.

### Bridge to Success content: Copyright 2011 The Open University.
* License: <http://creativecommons.org/licenses/by/3.0/>
* Source: <http://labspace.open.ac.uk/b2s>

### CodeIgniter: Copyright (c) 2008 - 2012, EllisLab, Inc.
* License: <http://codeigniter.com/user_guide/license.html>
* Code: <https://github.com/EllisLab/CodeIgniter/commit/e35658b5af498ff3>
* Based on: CodeIgniter, 2.1.2-0-ge35658b (v2.1.2), wesbaker, 2012-06-29.

### Layout library: Copyright 2006-2009, CI Toby, adaykin and others.
* License: Creative Commons Attribution-Share Alike License.
* Code: <http://codeigniter.com/wiki/layout_library>

### codeigniter-piwik: Copyright 2010 Bryce Johnston
* License: MIT
* Code: <https://github.com/wingdspur/codeigniter-piwik>

### php-po-parser: Copyright (C) 2008, Iulian Ilea (http://iulian.net), all rights reserved.
* License: <http://gnu.org/licenses/gpl.html> GNU GPL v3 onwards (code), or GNU Lesser GPL (project page)
* Code: <http://code.google.com/p/php-po-parser/>

### oer_license_parser.js: Copyright (C) 2011 by Pat Lockley.
* License: MIT
* Author: <patrick.lockley@googlemail.com>
* Code: <https://github.com/patlockley/openattribute-chrome>
* Via:  <https://github.com/tatemae/capret/tree/master/public/js>

### parseuri.js / 1.2.2: (c) Steven Levithan <stevenlevithan.com>
* License: MIT
* Code: <http://stevenlevithan.com/demo/parseuri/js/assets/parseuri.js>
* Via:  <http://blog.stevenlevithan.com/archives/parseuri>

### parseurl-dom.js / parseURL: Copyright 2009-02-19 James Padolsey
* License: <http://james.padolsey.com/terms-conditions/> public domain
* License: <http://unlicense.org/#the-unlicense>
* Code: <http://james.padolsey.com/javascript/parsing-urls-with-the-dom/>

### buster.js: @license MIT License (c) copyright B Cavalier & J Hann
* License: MIT
* Code: <http://busterjs.org/>

### Ender/jeesh.js: copyright Dustin Diaz & Jacob Thornton 2011 (@ded @fat)
* License: MIT
* Code: <http://ender.no.de/#jeesh>

### CaPReT public Javascript: CaPR�T was developed by Tatemae and the [MIT Office of Educational Innovation and Technology](http://oeit.mit.edu)
* License: MIT
* Code: <https://github.com/tatemae/capret/tree/master/public/js>

### Google Analytics JS v1/gajs.js: Copyright (c) 2009 Remy Sharp remysharp.com
* License: MIT
* Code: <http://code.google.com/p/google-analytics-js#r2>

### ierange.js: Copyright (c) 2009 Tim Cameron Ryan
* License: MIT
* Code: <http://code.google.com/p/ierange#r32>

### yUML.me: Copyright Tobin Harris/ Engineroom.
 * Tool: <http://yuml.me>
 * Online tool for UML class diagrams.


[End.]
